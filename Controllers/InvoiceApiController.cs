﻿using Marvin.JsonPatch;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using TestingAssignmentApp.Data;
using TestingAssignmentApp.Models;

namespace TestingAssignmentApp.Controllers
{
    public class InvoiceApiController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: api/InvoiceApi
        public IQueryable<Invoice> GetUnpaidInvoices()
        {
            CheckAuthorization();
            return db.Invoices.Where(x => x.Paid == false);
        }

        // POST: api/InvoiceApi
        [ResponseType(typeof(void))]
        public IHttpActionResult PayInvoice(int id)
        {
            CheckAuthorization();
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return NotFound();
            }
            invoice.Paid = true;

            db.Entry(invoice).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: api/InvoiceApi/5
        [HttpPatch]
        [ResponseType(typeof(void))]
        public IHttpActionResult Patch(int id, [FromBody] JsonPatchDocument<Invoice> patchEntity)
        {
            CheckAuthorization();
            if (patchEntity != null)
            {
                Invoice invoice = db.Invoices.Find(id);
                if (invoice == null)
                {
                    return NotFound();
                }

                patchEntity.ApplyTo(invoice);

                db.Entry(invoice).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InvoiceExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CheckAuthorization()
        {
            if (!Request.Headers.Contains("Authorization"))
            {
                throw new UnauthorizedAccessException("Missing Authorization header");
            }
            else if (Request.Headers.GetValues("Authorization").FirstOrDefault() != "authToken")
            {
                throw new UnauthorizedAccessException("Bad token!");
            }
            return true;
        }

        private bool InvoiceExists(int id)
        {
            return db.Invoices.Count(e => e.Id == id) > 0;
        }
    }
}