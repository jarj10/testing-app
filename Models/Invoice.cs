﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestingAssignmentApp.Models
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }

        [Required, DisplayName("Částka bez DPH")]
        public float BaseAmount { get; set; }

        [Required, DisplayName("DPH v %")]
        public float VATPercent { get; set; }

        [DisplayName("Placeno?")]
        public bool Paid { get; set; }

        [DisplayName("Částka DPH")]
        public float VATAmount
        {
            get
            {
                if (VATPercent == 0)
                {
                    return VATPercent;
                }
                return BaseAmount * (VATPercent / 100);
            }
        }

        [DisplayName("Celková částka včetně DPH")]
        public float Amount
        {
            get
            {
                return BaseAmount + VATAmount;
            }
        }
    }
}